
import comunicacoes.rmi.core.ServidorImpl;
import comunicacoes.rmi.visoes.ControleUniversal;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author necio
 */
public class Princpal {
    
    public static void main(String args[]){
        
       String str = (String) JOptionPane.showInputDialog(null, "Deseja iniciar um servidor ou um cliente?", "Sensor/Atuador", JOptionPane.QUESTION_MESSAGE,
                null, new String[]{"Servidor", "Cliente"} , null);
        
        if (str.equals("Servidor")){
            
            ServidorImpl serv = new ServidorImpl();
            serv.run();
            
        }else if (str.equals("Cliente")){            
            
            ControleUniversal c = new ControleUniversal();
            c.setVisible(true);
            c.setLocationRelativeTo(null);
                        
        }
               
    }
    
}

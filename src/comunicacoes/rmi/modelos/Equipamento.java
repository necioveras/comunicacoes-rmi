/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.rmi.modelos;

/**
 *
 * @author necio
 */
public abstract class Equipamento {
    
    private boolean status; 
    
    public Equipamento(){
        status = false;
    }
    
    public void ligar(){
        status = true;
    }
    
    public void desligar(){
        status = false;
    }
    
    public boolean isLigado(){
        return status;
    }
    
}

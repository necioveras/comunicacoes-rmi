package comunicacoes.rmi.core;

import comunicacoes.rmi.modelos.Ar;
import comunicacoes.rmi.modelos.Lampadas;
import comunicacoes.rmi.modelos.Luminosidade;
import comunicacoes.rmi.modelos.Passos;
import comunicacoes.rmi.modelos.Temperatura;
import comunicacoes.rmi.modelos.Tv;
import comunicacoes.rmi.modelos.Umidade;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class ServidorImpl extends Thread implements IServidor {
	
    private Tv tv;
    private Ar ar;
    private Lampadas lampadas;
    private String host;
    
    public ServidorImpl(String host){
        tv = new Tv();
        ar = new Ar();
        lampadas = new Lampadas();
        this.host = host;
    }
    
    public ServidorImpl(){
        tv = new Tv();
        ar = new Ar();
        lampadas = new Lampadas();
        this.host = "localhost";
    }
    
        
    @Override
    public void run() {        
            String nome = "Casa_Automatizada_RMI";
            Remote remote = new ServidorImpl();
            IServidor stub = null;

            try{
                    stub = (IServidor) UnicastRemoteObject.exportObject(remote, 0);
                    Registry reg = null;
                    try{
                            reg = LocateRegistry.createRegistry(8888);
                    } catch (RemoteException r){
                            reg = LocateRegistry.getRegistry(host ,8888);
                    }
                    reg.rebind(nome, stub);
                    System.out.println(nome + " em operação!");
            } catch (RemoteException rem){
                    rem.printStackTrace();
            }            
    }    
   

    @Override
    public synchronized void start() {
        super.start(); 
    }

    @Override
    public int getValor(String sensor) throws RemoteException {
        switch (sensor){
            case "passos": return new Passos().getNumero();
            case "umidade": return new Umidade().getNumero();
            case "temperatura": return new Temperatura().getNumero();
            case "luminosidade": return new Luminosidade().getNumero();
        }
        return 0;
    }

    @Override
    public void ligar(String device) throws RemoteException {
        switch (device){
            case "tv": tv.ligar(); break;
            case "ar": ar.ligar(); break;
            case "lampadas": lampadas.ligar(); break;
        }
        System.out.println(device + " acabou de ser ligado.");
    }

    @Override
    public void desligar(String device) throws RemoteException {
        switch (device){
            case "tv": tv.desligar(); break;
            case "ar": ar.desligar(); break;
            case "lampadas": lampadas.desligar(); break;
        }
        System.out.println(device + " acabou de ser desligado.");
    }
    
    @Override
    public boolean getStatus(String device) throws RemoteException {
        switch (device){
            case "tv": return tv.isLigado();
            case "ar": return ar.isLigado();
            case "lampadas": return lampadas.isLigado();
        }
        return false;
    }

}

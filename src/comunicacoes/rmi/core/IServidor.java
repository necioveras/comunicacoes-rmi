package comunicacoes.rmi.core;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServidor extends Remote {
	
    /*
	public int getPassos() throws RemoteException;
        public int getUmidade() throws RemoteException;
        public int getTemperatura() throws RemoteException;
        public int getLuminosidade() throws RemoteException;
      */
    
        public int  getValor(String sensor) throws RemoteException;
        
        public boolean getStatus(String device) throws RemoteException;
        public void ligar(String device) throws RemoteException;
        public void desligar(String device) throws RemoteException;
	

}
